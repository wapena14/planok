<?php 

$html = "<button type='button' onclick='location=\"../cotizaciones\"'>Volver</button>  <table  class='table table-striped'>
<tr><th colspan='9' style='text-align:center;'><h4>Productos</h4></tr>
<tr>
  <th>Tipo Producto</th>
  <th>Descripcion</th>
  <th>Orientacion</th>
  <th>Piso</th>
  <th>Superficie</th>
  <th>Sector</th>
</tr>";

foreach($data as $row){
  $id = $row['idcotizacion'];
  $descuento = $row['descuento'];
  $subtotal = $row['subtotal'];
  $total = $row['total'];
  $fechacreacion = $row['fechacreacion'];
  $estado = $row['estado'];
  $credito = $row['credito'];
  $rut_cliente = $row['rut_cliente'];
  $nombre_cliente = $row['nombre_cliente'];
  $telefono = $row['telefono'];
  $email = $row['email'];
  $edad_cliente = $row['edad_cliente'];
  $sexo_cliente = $row['sexo_cliente'];
  $region = $row['region'];
  $rut_usuario = $row['rut_usuario'];
  $nombre_usuario = $row['nombre_usuario'];
  $apellido_usuario = $row['apellido_usuario'];
  $correo = $row['correo'];
  
  $html .= "<tr>
   <td>".$row['descripcion_tproducto']."</td>
   <td>".$row['descripcion_producto']."</td>
   <td>".$row['orientacion']."</td>
   <td>".$row['piso']."</td>
   <td>".$row['superficie']."</td>
   <td>".$row['sector']."</td>
   </tr>";
}
$cabecera = "<table class='table table-bordered'>
<thead>
  <tr><th colspan='4' style='text-align:center;'><h4>Cliente</h4></tr>
</thead>
<tbody>
  <tr>
    <td>Id Cliente: <br> $rut_cliente</td>
    <td>Nombre: <br> $nombre_cliente</td>
    <td>Telefono: <br> $telefono</td>
    <td>Email Cliente: <br> $email</td>
  </tr>
  <tr>
    <td>Edad: <br> $edad_cliente años</td>
    <td>Sexo: <br> $sexo_cliente </td>
    <td>Region: <br> $region</td>
    <td></td>
  </tr>
  <tr><th colspan='4' style='text-align:center;'><h4>Usuario</h4></tr>
  <tr>
    <td>Rut: <br> $rut_usuario </td>
    <td>Nombre: <br> $nombre_usuario</td>
    <td>Apellido: <br> $apellido_usuario</td>
    <td>Email: <br> $correo</td>
  </tr>
  <tr><th colspan='4' style='text-align:center;'><h4>Cotización</h4></tr>
  <tr>
    <td>N°: <br> $id </td>
    <td>Descuento: <br> $descuento</td>
    <td>Subtotal: <br> $subtotal</td>
    <td>Total: <br> $total</td>
  </tr>
  <tr>
    <td>Fecha: <br> $fechacreacion </td>
    <td>Estado: <br> $estado</td>
    <td>Credito: <br> $credito</td>
    <td>Monto Credito: <br> $montocredito</td>
  </tr>
</tbody>
</table>";

echo $cabecera;
echo $html;
?>
</table>