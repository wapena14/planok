<table  class="table table-striped">
<tr><th colspan='9' style="text-align:center;"><h4>Clientes que han comprado estacionamientos en Santiago</h4></tr>
<?php 
    echo "<tr><th colspan='9' style='text-align:left;'><h6>SQL: ".$data['query_cliente']."</h6></tr>";
 ?>
<tr>
  <th>Rut</th>
  <th>Nombre</th>
  <th>Telefono</th>
  <th>Email</th>
  <th>Edad</th>
  <th>Sexo</th>
  <th>Region</th>
</tr>
<?php

foreach($data['clientes'] as $row){
  echo "<tr>";
  echo "<td>".$row['rut']."</td>";
  echo "<td>".$row['nombre']."</td>";    
  echo "<td>".$row['telefono']."</td>";
  echo "<td>".$row['email']."</td>";
  echo "<td>".$row['edad']."</td>";
  echo "<td>".$row['sexo']."</td>";
  echo "<td>".$row['region']."</td>";
  echo "</tr>";
}

?>
</table>

<?php

    echo "<h6>SQL: ".$data['query_departamentos']."</h6>";
    foreach($data['departamentos'] as $row){
        echo "<h4>Total, de departamentos Vendidos por el usuario PILAR PINO en Las Condes: <u>".$row['total']."</u></h4>";
    } 
 ?>


<table  class="table table-striped">
<tr><th colspan='9' style="text-align:center;"><h4>Productos creados entre el 2018-01-03 y 2018-01-20</h4></tr>
<?php 
    echo "<tr><th colspan='9' style='text-align:left;'><h6>SQL: ".$data['query_fecha']."</h6></tr>";
 ?>
<tr>
  <th>Tipo Producto</th>
  <th>Descripcion</th>
  <th>Orientacion</th>
  <th>Piso</th>
  <th>Superficie</th>
  <th>Sector</th>
  <th>Estado</th>
  <th>Fecha Creación</th>
</tr>
<?php

foreach($data['fecha'] as $row){
  echo "<tr>";
  echo "<td>".$row['descripcion_tipo']."</td>";
  echo "<td>".$row['descripcion']."</td>";    
  echo "<td>".$row['orientacion']."</td>";
  echo "<td>".$row['piso']."</td>";
  echo "<td>".$row['superficie']."</td>";
  echo "<td>".$row['sector']."</td>";
  echo "<td>".$row['estado']."</td>";
  echo "<td>".$row['fechaCreacion']."</td>";
  echo "</tr>";
}

?>
  </table>

<?php
    echo "<h6>SQL: ".$data['query_ventas']."</h6>";
    foreach($data['ventas'] as $row){
        echo "<h4>Total de ventas realizadas en Santiago: <u>".$row['valor']."</u></h4>";
    } 
?>