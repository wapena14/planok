<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<?php include "includes/scripts.php"; ?>
	<title>Sistema PLAN OK</title>
</head>
<body>
<?php
include "includes/header.php";
?>
<section id="container">
<?php
class Views{

    function default(){
        echo '<h1>Bienvenido</h1>';
    }

    function usuarios($data){
        //echo '<h1>usuaiross</h1>';
        include "includes/usuarios.php";
    }

    function cotizaciones($data){
        include "includes/cotizaciones.php";
    }

    function consulta($data){
        include "includes/consultas.php";
    }

    function detalle($data){
        include "includes/detalle.php";

    }

}

?>
</body>
</html>