# prueba-planok

<br/>

<div style="text-align:center;">

![PHP](https://img.shields.io/badge/-PHP-333333?style=flat&logo=docker)

</div>

<br/>

>## Instalación

<br/>

_Proyecto:_

```bash
git clone https://gitlab.com/wapena14/planok.git
cd planok
```

>## Configuración

Cambiar los parámetros de conexión a la base de datos en el archivo
```
models\include\conexion.php
```


>## Validación

Validar que en la carpeta de donde se descargó el proyecto se encuentre el archivo .htaccess 

En caso de que no exista el archivo crearlo con la siguiente estructura: 

```
RewriteEngine On
RewriteRule ^([a-zA-Z_-]*)$ index.php?modulo=$1
RewriteRule ^([a-zA-Z_-]*)/([0-9-]*)$ index.php?modulo=$1&id=$2 [L,QSA]
```
