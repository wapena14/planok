<?php

class Controller{
    static function main(){
        require "./models/Models.php";
        require "./views/Views.php";

		$modulo = $_GET['modulo'];
        $id = $_GET['id'];
        $model = new Models();
        $view = new Views();
        
        switch ($modulo) {
            case "usuarios":
                $usuarios = $model->usuarios();
                $view->usuarios($usuarios);
                break;       
            case "cotizaciones":
                if(!strlen($id)){
                    $cotizaciones = $model->cotizaciones();
                    $view->cotizaciones($cotizaciones);
                }else{
                   
                    $detalle = $model->detalle($id);
                    $view->detalle($detalle);
                }
                
                break;
            case "consultas":
                $consultas = $model->consultas(); 
                $view->consulta($consultas);
                break;
            default:
                $view->default();
                break;
        }


    }
}

?>