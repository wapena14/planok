<?php
include_once("include/conexion.php");
class Models{

    function __construct(){
        $this->conexion = new Database();
    }
    
    
    function usuarios(){
        //$conexion = new Database();
        
        $query = "SELECT id, rut, descripcion, nombre, apellido, correo, edad, sexo, estado from usuario inner join perfil on usuario.idperfil = perfil.idperfil ORDER BY 1 asc;";     
        return $this->conexion->query($query);

    }

    function cotizaciones(){
        $query = "SELECT idcotizacion, rut, subtotal, descuento, total from cotizacion inner join cliente on cotizacion.idcliente = cliente.id ORDER BY 1 asc;";
        return $this->conexion->query($query);

    }

    function detalle($id){
        $query = "SELECT cotizacion.idcotizacion, descuento, subtotal, total, cotizacion.fechacreacion, cotizacion.estado as estado, credito, 
        montocredito, cliente.rut as rut_cliente, cliente.nombre as nombre_cliente, telefono, email, 
        cliente.edad as edad_cliente, cliente.sexo as sexo_cliente, region, usuario.rut as rut_usuario, 
        usuario.nombre as nombre_usuario, usuario.apellido as apellido_usuario, correo, 
        producto.descripcion as descripcion_producto, orientacion, piso, superficie, sector, tipo_producto.descripcion as descripcion_tproducto
        from cotizacion inner join cliente on cotizacion.idcliente = cliente.id
        inner join usuario on cotizacion.idusuario = usuario.id
        inner join perfil on perfil.idperfil = usuario.idperfil
        left join cotizacion_producto on cotizacion.idcotizacion = cotizacion_producto.idcotizacion
        inner join producto on producto.idproducto = cotizacion_producto.idproducto
        inner join tipo_producto on tipo_producto.idtipoproducto = producto.idtipoproducto 
        where cotizacion_producto.idcotizacion = '$id'; ";
        return $this->conexion->query($query);
    }

    function consultas(){

        $clientes = "select cl.* from producto p inner join tipo_producto tp on p.idtipoproducto = tp.idtipoproducto 
		inner join cotizacion_producto cp on cp.idproducto = p.idproducto inner join cotizacion c on c.idcotizacion = cp.idcotizacion
		inner join cliente cl on cl.id = c.idcliente
		where tp.descripcion = 'Estacionamiento' and p.sector = 'Santiago' and p.estado = 'VENDIDO';";

        $departamentos = "select count(*) as total from producto p inner join tipo_producto tp on p.idtipoproducto = tp.idtipoproducto 
		inner join cotizacion_producto cp on cp.idproducto = p.idproducto inner join cotizacion c on c.idcotizacion = cp.idcotizacion
		inner join usuario u on u.id = c.idcliente
		where u.nombre = 'PILAR' and u.apellido = 'PINO' and p.sector = 'Las Condes' and p.estado = 'VENDIDO' and tp.descripcion = 'Departamentos';";

        $fecha = "select p.*, tp.descripcion as descripcion_tipo from producto p inner join tipo_producto tp on p.idtipoproducto = tp.idtipoproducto
        where p.fechacreacion BETWEEN '2018-01-03' and '2018-01-20' order by fechacreacion;";

        $ventas = "select sum(valorLista) as valor from producto p where p.estado = 'VENDIDO' and p.sector = 'Santiago';";

        
        return array(
            "clientes" => $this->conexion->query($clientes),
            "query_cliente" => $clientes,
            "departamentos" => $this->conexion->query($departamentos),
            "query_departamentos" => $departamentos,
            "fecha" => $this->conexion->query($fecha),
            "query_fecha" => $fecha,
            "ventas" => $this->conexion->query($ventas),
            "query_ventas" => $ventas
        );    
    }

}

?>